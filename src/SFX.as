package
{
	import flash.media.Sound;
	import flash.media.SoundChannel;

	public class SFX
	{
		[Embed(source="assets/sfx/Beep_tim-Matthieu-8588_hifi.mp3")]
		private static var HomeSound:Class;
		private static var homeSound:Sound = new HomeSound() as Sound; 
		private static var sndChannel:SoundChannel;
		
		public static const BUTTON_BEEP:String = "btnBeep";
		
		public function SFX(){}
		
		public static function play(id:String):void {

			if (sndChannel){
				sndChannel.stop();
			}
			
			switch(id){
				case BUTTON_BEEP:
					sndChannel =  homeSound.play();
					break;
			}
		}
	}
}