package
{
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.Event;
	
	public class DotsBackground extends Sprite
	{
		[Embed(source="assets/img/dotsTile.png")]
		private var DotsTile:Class;
		
		public function DotsBackground()
		{
			super();
			this.addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			
		}
		
		private function onAddedToStage(event:Event):void {
			this.removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			
			var dotsBmp:Bitmap = new DotsTile();
			this.graphics.beginBitmapFill(dotsBmp.bitmapData,null,true);
			this.graphics.drawRect(0,0,stage.stageWidth, stage.stageHeight);
			this.graphics.endFill();
		}
	}
}	