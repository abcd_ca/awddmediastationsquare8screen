package
{
	import ca.stationsquare.wallmaps.ApplicationFacade;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;

	[SWF(frameRate="30",backgroundColor="0x000000",width="1920",height="2160")]
	public class Main extends Sprite
	{
		public function Main()
		{
			super();
			ApplicationFacade.getInstance().startup(this);
		}
	}
}