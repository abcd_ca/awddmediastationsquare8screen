package ca.stationsquare.wallmaps
{
	public class Notifications
	{
		public static const STARTUP								:String = "startup";   
        public static const STARTUP_COMPLETE					:String = "startupComplete";   
//        public static const CONFIG_LOADED						:String = "configLoaded";
//		
		public static const NAVIGATE_TO_SCREEN					:String = "navigateToScreen";
		public static const NAVIGATE_TO_MAP_SECTION				:String = "navigateToMapSection";
		public static const SERVER_LOG_MESSAGE					:String = "serverLogMessage";
		public static const REMOTE_TAP							:String = "remoteTap";
		public static const APP_IDLE_TIMEOUT					:String = "idleTimeout";
//		public static const NAVIGATE_TO_SLIDE					:String = "navigateToSlide";
//		
//		public static const CANCEL_NARRATION					:String = "cancelNarration";
//		public static const PLAY_NARRATION						:String = "playNarration";
//		public static const PAUSE_NARRATION						:String = "pauseNarration";
////		public static const PLAY_COLLECTIBLE_NARRATION			:String = "playCollectibleNarration";
////		public static const PLAY_HINT_NARRATION					:String = "playHintNarration";
//		public static const NARRATION_COMPLETE					:String = "narrationComplete";
//		public static const PLAY_SOUND_EFFECT					:String = "playSoundEffect";
//
//		public static const SLIDESHOW_COMPLETE					:String = "slideshowComplete";
//		public static const RESET								:String = "reset";
//
//		public static const OPEN_CROSS_MARKETING_SITE			:String = "openCrossMarketingSite";
//
//		public static const SKIP_AHEAD							:String = "skipAhead";
//		public static const HELP_VIDEO_COMPLETE					:String = "helpVideoComplete";
//
//		public static const BLOCK_INPUT							:String = "blockInput";
//		public static const UNBLOCK_INPUT						:String = "unblockInput";
//		public static const NARRATION_STATE_CHANGE				:String = "narrationStateChange";
//
//		/**
//		 * For when app resumes from being in the background on iOS
//		 */
//		public static const ACTIVATE							:String = "activate";
//		public static const RESUME_FROM_BACKGROUND				:String = "resumeFromBackground";
	}
}