package ca.stationsquare.wallmaps.controller
{
	import ca.stationsquare.wallmaps.model.ConfigProxy;
	import ca.stationsquare.wallmaps.view.ApplicationMediator;
	import ca.stationsquare.wallmaps.view.HomeViewMediator;
	import ca.stationsquare.wallmaps.view.MainViewMediator;
	import ca.stationsquare.wallmaps.view.MapViewMediator;
	import ca.stationsquare.wallmaps.view.component.MainView;
	
	import caurina.transitions.properties.ColorShortcuts;
	import caurina.transitions.properties.DisplayShortcuts;
	
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageDisplayState;
	import flash.display.StageScaleMode;
	import flash.events.TimerEvent;
	import flash.ui.Mouse;
	import flash.utils.Timer;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.AsyncCommand;
	import org.puremvc.as3.patterns.command.SimpleCommand;
	
    /**
     * Prepare the View for use.
     * 
     * <P>
     * The <code>Notification</code> was sent by the <code>Application</code>,
     * and a reference to that view component was passed on the note body.
     * The <code>ApplicationMediator</code> will be created and registered using this
     * reference. The <code>ApplicationMediator</code> will then register 
     * all the <code>Mediator</code>s for the components it created.</P>
     * 
     */
    public class ViewPrepCommand extends AsyncCommand
    {
        override public function execute( note:INotification ) :void    
		{
			DisplayShortcuts.init();
//			ColorShortcuts.init();
			/*
			SoundShortcuts.init();
			*/
			
            // Register the MainScreenMediator
            var mainApp:Sprite = note.getBody() as Sprite;
			
			//for dev testing
			var configProxy:ConfigProxy = facade.retrieveProxy(ConfigProxy.NAME) as ConfigProxy;
			
			mainApp.stage.align = StageAlign.TOP_LEFT;
			
			//go full screen. Without this delay it doesn't work.
			if (configProxy.fullScreen){
				var t:Timer = new Timer(.5,1);
				t.addEventListener(TimerEvent.TIMER_COMPLETE, function():void { mainApp.stage.displayState = StageDisplayState.FULL_SCREEN});
				t.start();
			}
			
			if (!configProxy.showMousePointer){
				Mouse.hide()
			}
			
            var mainView:MainView = new MainView(configProxy.showGuides);
			mainApp.stage.scaleMode = configProxy.scaleMode;
			if (configProxy.scaleMode == StageScaleMode.SHOW_ALL){
				var mask:Shape = new Shape();
				mask.graphics.beginFill(0xff0000);
				mask.graphics.drawRect(0,0,1920,2160);
				mask.graphics.endFill();
				mainApp.addChild(mask);
				
				mainView.mask = mask;
			}
			
			mainApp.addChild(mainView);
            
            facade.registerMediator( new ApplicationMediator( mainApp ) );
            facade.registerMediator( new MainViewMediator( mainView) );
            facade.registerMediator( new HomeViewMediator( mainView.homeView) );
            facade.registerMediator( new MapViewMediator( mainView.mapView) );
//            facade.registerMediator( new SoundViewMediator( new SoundView() ) );
            //facade.registerMediator( new WelcomeScreenMediator( mainView ) );
			
			this.commandComplete();
		}
    }
}
