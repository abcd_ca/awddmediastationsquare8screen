package ca.stationsquare.wallmaps.controller.navigate
{
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.AsyncMacroCommand;
	
	/**
	 */
	public class NavigateToMapSectionCommand extends AsyncMacroCommand
	{
		override protected function initializeAsyncMacroCommand():void
		{
			addSubCommand( TearDownMapSectionCommand);
			addSubCommand( BuildUpMapSectionCommand);
		}
	}
}