package ca.stationsquare.wallmaps.controller.navigate
{
	
	import ca.stationsquare.wallmaps.event.TransitionEvent;
	import ca.stationsquare.wallmaps.model.ConfigProxy;
	import ca.stationsquare.wallmaps.model.MapSectionID;
	import ca.stationsquare.wallmaps.model.ScreenID;
	import ca.stationsquare.wallmaps.model.StateProxy;
	import ca.stationsquare.wallmaps.model.TimeoutProxy;
	import ca.stationsquare.wallmaps.view.MainViewMediator;
	import ca.stationsquare.wallmaps.view.MapViewMediator;
	import ca.stationsquare.wallmaps.view.component.MainView;
	import ca.stationsquare.wallmaps.view.component.MapView;
	
	import flash.display.Screen;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.AsyncCommand;
	
	/**
	 * Create and register <code>Proxy</code>s with the <code>Model</code>.
	 */
	public class TearDownMapSectionCommand extends AsyncCommand
	{
		private var _mapView:MapView;
		
		override public function execute( note:INotification ) :void    
		{
			trace("TearDownMapSectionCommand");
			var stateProxy:StateProxy = facade.retrieveProxy(StateProxy.NAME) as StateProxy;
			var timeoutProxy:TimeoutProxy = facade.retrieveProxy(TimeoutProxy.NAME) as TimeoutProxy;
			timeoutProxy.stop();
			if (stateProxy.currentScreenID == ScreenID.MAP){
				_mapView = MapViewMediator(facade.retrieveMediator(MapViewMediator.NAME)).mapView;
				_mapView.addEventListener(TransitionEvent.TEAR_DOWN_SECTION_COMPLETE, onTearDownSectionComplete);
				_mapView.currentMapSectionID = stateProxy.nextMapSectionID;
				_mapView.tearDownSection();
			}else{
				this.commandComplete();
			}
		}
		
		private function onTearDownSectionComplete(event:TransitionEvent):void {
			_mapView.removeEventListener(TransitionEvent.TEAR_DOWN_SECTION_COMPLETE, onTearDownSectionComplete);
			var stateProxy:StateProxy = facade.retrieveProxy(StateProxy.NAME) as StateProxy;
			var configProxy:ConfigProxy = facade.retrieveProxy(ConfigProxy.NAME) as ConfigProxy;
			stateProxy.currentMapSectionID = stateProxy.nextMapSectionID;
			stateProxy.nextMapSectionID = null;
			
			_mapView.setSectionData( configProxy.getSectionDataForSectionID(stateProxy.currentMapSectionID) )
			
			this.commandComplete();
		}
	}
}