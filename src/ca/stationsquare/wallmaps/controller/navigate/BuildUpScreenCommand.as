package ca.stationsquare.wallmaps.controller.navigate
{
	import ca.stationsquare.wallmaps.event.TransitionEvent;
	import ca.stationsquare.wallmaps.model.StateProxy;
	import ca.stationsquare.wallmaps.model.TimeoutProxy;
	import ca.stationsquare.wallmaps.view.MainViewMediator;
	import ca.stationsquare.wallmaps.view.component.MainView;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.AsyncCommand;
	
	/**
	 * Create and register <code>Proxy</code>s with the <code>Model</code>.
	 */
	public class BuildUpScreenCommand extends AsyncCommand
	{
		private var _mainView:MainView;
		
		override public function execute( note:INotification ) :void    
		{
			var stateProxy:StateProxy = facade.retrieveProxy(StateProxy.NAME) as StateProxy;
			if (stateProxy.currentScreenID){
				_mainView = MainViewMediator(facade.retrieveMediator(MainViewMediator.NAME)).mainView;
				_mainView.currentView.addEventListener(TransitionEvent.BUILD_UP_COMPLETE, this.onBuildUpComplete);
				_mainView.currentView.buildUp();
			}else{
				this.commandComplete();
			}
		}
		
		private function onBuildUpComplete(event:TransitionEvent):void {
			_mainView.currentView.removeEventListener(TransitionEvent.BUILD_UP_COMPLETE, this.onBuildUpComplete);
			var stateProxy:StateProxy = facade.retrieveProxy(StateProxy.NAME) as StateProxy;
			var timeoutProxy:TimeoutProxy = facade.retrieveProxy(TimeoutProxy.NAME) as TimeoutProxy;
			timeoutProxy.start();
			this.commandComplete();
		}
	}
}