package ca.stationsquare.wallmaps.controller.navigate
{
	import ca.stationsquare.wallmaps.model.ConfigProxy;
	import ca.stationsquare.wallmaps.model.ScreenID;
	import ca.stationsquare.wallmaps.model.StateProxy;
	import ca.stationsquare.wallmaps.view.MainViewMediator;
	import ca.stationsquare.wallmaps.view.component.MainView;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.AsyncCommand;
	
	/**
	 * Create and register <code>Proxy</code>s with the <code>Model</code>.
	 */
	public class ChangeScreenCommand extends AsyncCommand
	{
		override public function execute( note:INotification ) :void    
		{
			var stateProxy:StateProxy = facade.retrieveProxy(StateProxy.NAME) as StateProxy;
			var configProxy:ConfigProxy = facade.retrieveProxy(ConfigProxy.NAME) as ConfigProxy;
			
			var mainView:MainView = MainViewMediator(facade.retrieveMediator(MainViewMediator.NAME)).mainView;
			
			switch(stateProxy.currentScreenID){
				case ScreenID.HOME:
					mainView.removeChild(mainView.homeView);
					break;

				case ScreenID.MAP:
//					mainView.removeChild(mainView.mapView);
					break;
			}
			
			switch(stateProxy.nextScreenID){
				case ScreenID.MAP:
					mainView.currentView = mainView.mapView;
					stateProxy.currentMapSectionID = stateProxy.nextMapSectionID;
					stateProxy.nextMapSectionID = null;
					
					mainView.mapView.setSectionData(configProxy.getSectionDataForSectionID(stateProxy.currentMapSectionID));
					mainView.mapView.currentMapSectionID = stateProxy.currentMapSectionID;
					break;

				case ScreenID.HOME:
					mainView.currentView = mainView.homeView;
					mainView.addChild(mainView.homeView);
					break;
			}
			
			mainView.moveGuidesToFront();
			
			stateProxy.currentScreenID = stateProxy.nextScreenID;
			stateProxy.nextScreenID = null;
			
			this.commandComplete();
		}
	}
}