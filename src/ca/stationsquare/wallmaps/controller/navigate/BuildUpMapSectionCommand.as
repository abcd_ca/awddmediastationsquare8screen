package ca.stationsquare.wallmaps.controller.navigate
{
	import ca.stationsquare.wallmaps.event.TransitionEvent;
	import ca.stationsquare.wallmaps.model.ScreenID;
	import ca.stationsquare.wallmaps.model.StateProxy;
	import ca.stationsquare.wallmaps.model.TimeoutProxy;
	import ca.stationsquare.wallmaps.view.MainViewMediator;
	import ca.stationsquare.wallmaps.view.MapViewMediator;
	import ca.stationsquare.wallmaps.view.component.MainView;
	import ca.stationsquare.wallmaps.view.component.MapView;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.AsyncCommand;
	
	/**
	 * Create and register <code>Proxy</code>s with the <code>Model</code>.
	 */
	public class BuildUpMapSectionCommand extends AsyncCommand
	{
		private var _mapView:MapView;
		
		override public function execute( note:INotification ) :void    
		{
			trace("BuildUpMapSectionCommand");
			var stateProxy:StateProxy = facade.retrieveProxy(StateProxy.NAME) as StateProxy;
			if (stateProxy.currentScreenID == ScreenID.MAP){
				var timeoutProxy:TimeoutProxy = facade.retrieveProxy(TimeoutProxy.NAME) as TimeoutProxy;
				_mapView = MapViewMediator(facade.retrieveMediator(MapViewMediator.NAME)).mapView;
				_mapView.buildUpSection();
				
				timeoutProxy.start();
			}
			this.commandComplete();
		}
	}
}