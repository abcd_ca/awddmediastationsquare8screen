package ca.stationsquare.wallmaps.controller.navigate
{
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.AsyncMacroCommand;
	
	/**
	 */
	public class NavigateToScreenCommand extends AsyncMacroCommand
	{
		override protected function initializeAsyncMacroCommand():void
		{
			addSubCommand( TearDownMapSectionCommand);
			addSubCommand( TearDownScreenCommand );
			addSubCommand( ChangeScreenCommand );
			addSubCommand( BuildUpScreenCommand );
			addSubCommand( BuildUpMapSectionCommand );
		}
	}
}