package ca.stationsquare.wallmaps.controller.navigate
{
	
	import ca.stationsquare.wallmaps.event.TransitionEvent;
	import ca.stationsquare.wallmaps.model.ScreenID;
	import ca.stationsquare.wallmaps.model.StateProxy;
	import ca.stationsquare.wallmaps.model.TimeoutProxy;
	import ca.stationsquare.wallmaps.view.MainViewMediator;
	import ca.stationsquare.wallmaps.view.component.MainView;
	import ca.stationsquare.wallmaps.view.component.MapView;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.AsyncCommand;
	
	/**
	 * Create and register <code>Proxy</code>s with the <code>Model</code>.
	 */
	public class TearDownScreenCommand extends AsyncCommand
	{
		private var _mainView:MainView;
//		private var _soundProxy:SoundProxy
		
		override public function execute( note:INotification ) :void    
		{
			var stateProxy:StateProxy = facade.retrieveProxy(StateProxy.NAME) as StateProxy;
			var timeoutProxy:TimeoutProxy = facade.retrieveProxy(TimeoutProxy.NAME) as TimeoutProxy;
			timeoutProxy.stop();
//			stateProxy.isNavigating = true;
			
//			sendNotification(Notifications.BLOCK_INPUT);
//			_soundProxy = facade.retrieveProxy(SoundProxy.NAME) as SoundProxy;
			_mainView = MainViewMediator(facade.retrieveMediator(MainViewMediator.NAME)).mainView;
			var mapView:MapView = _mainView.mapView;
			if (stateProxy.currentScreenID){
				switch (true){
					case stateProxy.currentScreenID == ScreenID.HOME && stateProxy.nextScreenID == ScreenID.MAP:
//						mapView.reset();
						_mainView.addChildAt(_mainView.mapView, 0);
						break;
					
					case stateProxy.currentScreenID == ScreenID.MAP && stateProxy.nextScreenID == ScreenID.HOME:
						_mainView.addChild(_mainView.homeView);
						break;
				}
				_mainView.currentView.addEventListener(TransitionEvent.TEAR_DOWN_COMPLETE, this.onTearDownComplete);
				_mainView.currentView.tearDown();
			}else{
				this.commandComplete();
			}
		}
		
		private function onTearDownComplete(event:TransitionEvent):void {
			_mainView.currentView.removeEventListener(TransitionEvent.TEAR_DOWN_COMPLETE, this.onTearDownComplete);
//			_soundProxy.narrationClipState = null;
			this.commandComplete();
		}
	}
}