package ca.stationsquare.wallmaps.controller
{
	import ca.stationsquare.wallmaps.Notifications;
	import ca.stationsquare.wallmaps.model.ConfigProxy;
	import ca.stationsquare.wallmaps.model.TimeoutProxy;
	import ca.stationsquare.wallmaps.model.RemoteInputProxy;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.SimpleCommand;
	
    /**
     */
    public class StartupCompleteCommand extends SimpleCommand
    {
        override public function execute( note:INotification ) :void    
		{
			trace("startup complete command");
			var touchInputProxy:RemoteInputProxy = facade.retrieveProxy(RemoteInputProxy.NAME) as RemoteInputProxy;
			var configProxy:ConfigProxy = facade.retrieveProxy(ConfigProxy.NAME) as ConfigProxy;
			var timeoutProxy:TimeoutProxy = facade.retrieveProxy(TimeoutProxy.NAME) as TimeoutProxy;
			timeoutProxy.start();
			
			touchInputProxy.startServer(configProxy.serverPort);
			
            sendNotification(Notifications.STARTUP_COMPLETE);
        }
    }
}