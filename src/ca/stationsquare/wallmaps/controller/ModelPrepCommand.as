package ca.stationsquare.wallmaps.controller
{
	import ca.stationsquare.wallmaps.model.ConfigProxy;
	import ca.stationsquare.wallmaps.model.StateProxy;
	import ca.stationsquare.wallmaps.model.TimeoutProxy;
	import ca.stationsquare.wallmaps.model.RemoteInputProxy;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.AsyncCommand;
	import org.puremvc.as3.patterns.command.SimpleCommand;
	
    /**
     * Create and register <code>Proxy</code>s with the <code>Model</code>.
     */
    public class ModelPrepCommand extends AsyncCommand
    {
        override public function execute( note:INotification ) :void    
		{
            facade.registerProxy(new ConfigProxy());
            facade.registerProxy(new StateProxy());
            facade.registerProxy(new RemoteInputProxy());
            facade.registerProxy(new TimeoutProxy());
			
			this.commandComplete();
        }
    }
}