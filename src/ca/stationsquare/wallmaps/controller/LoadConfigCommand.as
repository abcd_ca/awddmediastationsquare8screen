package ca.stationsquare.wallmaps.controller
{
	import ca.stationsquare.wallmaps.model.ConfigProxy;
	import ca.stationsquare.wallmaps.model.MapSectionID;
	import ca.stationsquare.wallmaps.model.StateProxy;
	import ca.stationsquare.wallmaps.model.TimeoutProxy;
	
	import flash.events.Event;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.AsyncCommand;
	import org.puremvc.as3.patterns.command.SimpleCommand;
	
    /**
     * Create and register <code>Proxy</code>s with the <code>Model</code>.
     */
    public class LoadConfigCommand extends AsyncCommand
    {
        override public function execute( note:INotification ) :void    
		{
			var configFile:File = File.applicationDirectory;
			configFile = configFile.resolvePath("assets/config.xml");
			
			var fileStream:FileStream = new FileStream(); 
			fileStream.open(configFile, FileMode.READ); 
			
			try {
				var configXML:XML = XML(fileStream.readUTFBytes(fileStream.bytesAvailable));
				fileStream.close(); 
				
				var configProxy:ConfigProxy = facade.retrieveProxy(ConfigProxy.NAME) as ConfigProxy;
				var timeoutProxy:TimeoutProxy = facade.retrieveProxy(TimeoutProxy.NAME) as TimeoutProxy;
				configProxy.setData( configXML );
				
				timeoutProxy.setDurationInSeconds(configProxy.idleTimeoutSeconds);
				
			} catch (e:TypeError) {
				trace("Could not parse the XML file.");
			}
			
			this.commandComplete();
        }
    }
}