package ca.stationsquare.wallmaps.controller
{
    import org.puremvc.as3.interfaces.*;
    import org.puremvc.as3.patterns.command.*;

    /**
     * A MacroCommand executed when the application starts.
     *
       * @see ModelPrepCommand 
       * @see ViewPrepCommand
     */
    public class StartupCommand extends AsyncMacroCommand
    {
		override protected function initializeAsyncMacroCommand():void
        {
        	addSubCommand( ModelPrepCommand );
			addSubCommand( LoadConfigCommand );
        	addSubCommand( ViewPrepCommand );
			addSubCommand( StartupCompleteCommand );
        	//addSubCommand( InitAnalyticsCommand );
        }
    }
}
