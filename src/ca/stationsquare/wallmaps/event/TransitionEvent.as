package ca.stationsquare.wallmaps.event
{
	import flash.events.Event;
	
	public class TransitionEvent extends Event
	{
		public static const	BUILD_UP_COMPLETE	:String = "buildUpComplete";
		public static const	TEAR_DOWN_COMPLETE	:String = "tearDownComplete";
		
		public static const	BUILD_UP_SECTION_COMPLETE	:String = "BUILD_UP_SECTION_COMPLETE";
		public static const	TEAR_DOWN_SECTION_COMPLETE	:String = "TEAR_DOWN_SECTION_COMPLETE";
		
		public function TransitionEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
	}
}