package ca.stationsquare.wallmaps.event
{
	import flash.events.Event;
	
	public class MapViewEvent extends Event
	{
		public static const CLICK_ZOOM_TO_VANCOUVER:String = "clickZoomToVancouver";
		public static const CLICK_ZOOM_TO_BURNABY:String = "clickZoomToBurnaby";
		public static const CLICK_TRANSIT:String = "clickTransit";
		public static const CLICK_SHOPPING:String = "clickShopping";
		public static const CLICK_DINING:String = "clickDining";
		public static const CLICK_PERSONAL_SERVICES:String = "clickPersonalServices";
		public static const CLICK_EDUCATION:String = "clickEducation";
		public static const CLICK_ENTERTAINMENT:String = "clickEntertainment";
		
		public function MapViewEvent(type:String, bubbles:Boolean=true, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
	}
}