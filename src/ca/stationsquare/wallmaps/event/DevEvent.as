package ca.stationsquare.wallmaps.event
{
	import flash.events.Event;
	
	public class DevEvent extends Event
	{
		public var data:Object;
		
		public function DevEvent(type:String, data:Object = null, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			this.data = data;
		}
	}
}