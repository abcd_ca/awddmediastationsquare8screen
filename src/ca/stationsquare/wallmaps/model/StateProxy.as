package ca.stationsquare.wallmaps.model
{
	import org.puremvc.as3.interfaces.IProxy;
	import org.puremvc.as3.patterns.proxy.Proxy;
	
	/**
	 * Keeps track of certain application-level state attributes
	 */
	public class StateProxy extends Proxy implements IProxy
	{
		public static const NAME					:String = "StateProxy";
		
		public var currentScreenID:String;
		public var nextScreenID:String;
		
		public var currentMapSectionID:String;
		public var nextMapSectionID:String;
		
//		/**
//		 * Activate triggers when app starts up but also when returning from background on mobile. 
//		 * I only want to show the special, Resume/Continue dialog after activating from background.
//		 */
//		public var firstActivation:Boolean = true;
//		
//		/**
//		 * Is in the midst of a NavigateToScreen
//		 */
//		public var isNavigating:Boolean;
//		
//		/**
//		 * If it's a choice screen, int >= 0, otherwise -1
//		 */
//		public var currentChoiceScreenIndex:int = -1;
//
//		/**
//		 * If it's a slideshow screen, int >= 0, otherwise -1
//		 */
//		public var currentSlideshowSlideIndex:int = -1;
//		
//		/**
//		 * Supports different sound played on return visits to same choice screen
//		 */  
//		public var choiceScreenVisited:Boolean;
//		
//		/**
//		 * num clicks on the current practice screen
//		 */
//		public var practiceScreenClicks:uint;
		
		public function StateProxy( data:Object = null ) {
			super( NAME, data );
		}
		
//		public function reset():void {
//			this.currentChoiceScreenIndex = -1;
//			this.currentSlideshowSlideIndex = -1;
//			this.choiceScreenVisited = false;
//			this.practiceScreenClicks = 0;
//			this.isNavigating = false;
//		}
	}
}