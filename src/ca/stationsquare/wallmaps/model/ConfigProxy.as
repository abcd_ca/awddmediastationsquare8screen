package ca.stationsquare.wallmaps.model
{
	import flash.display.StageScaleMode;
	
	import org.puremvc.as3.interfaces.IProxy;
	import org.puremvc.as3.patterns.proxy.Proxy;
	
	/**
	 * Keeps track of certain application-level state attributes
	 */
	public class ConfigProxy extends Proxy implements IProxy
	{
		public static const NAME					:String = "ConfigProxy";
		
		public function ConfigProxy( data:Object = null ) {
			super( NAME, data );
		}
		
		public function get scaleMode():String {
			var mode:String;
			if (configXML.dev.scaleMode == "SHOW_ALL"){
				mode = StageScaleMode.SHOW_ALL;
			}else{
				mode = StageScaleMode.NO_SCALE;
			}
			return mode;
		}
		
		public function get fullScreen():Boolean {
			return configXML.dev.fullScreen == "true";
		}
		
		public function get showServerLog():Boolean {
			return configXML.dev.showServerLog == "true";
		}
		
		public function get showMousePointer():Boolean {
			return configXML.dev.showMousePointer == "true";
		}
		
		public function get showGuides():Boolean {
			return configXML.dev.showGuides == "true";
		}
		
		public function get serverPort():int {
			return int(configXML.server.port);
		}
		
		private function get configXML():XML {
			return data as XML;
		}
		
		public function getSectionDataForSectionID(id:String):XML {
			return configXML.content.section.(@id == id)[0];
		}
		
		public function get idleTimeoutSeconds():Number {
			var timeout:Number = Number(configXML.idleTimeoutSeconds);
			if (timeout < 5){
				trace("timeout is set to " + timeout + " seconds. That's too low, defaulting to 60 seconds");
				timeout = 60;
			}
			return timeout;
		}
		
		/**
		 * useful for development
		 */
		public function updatePlacemarkLocation(mapSectionID:String, index:int, x:int, y:int):void {
			var placemarkXML:XML = this.getSectionDataForSectionID(mapSectionID)..placemark[index] as XML;
			placemarkXML.@x = x;
			placemarkXML.@y = y;
			
			trace("------------------updated -------------------");
			trace(this.configXML.toXMLString());
		}
	}
}