package ca.stationsquare.wallmaps.model
{
	import ca.stationsquare.wallmaps.Notifications;
	
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	import org.puremvc.as3.interfaces.IProxy;
	import org.puremvc.as3.patterns.proxy.Proxy;
	
	/**
	 * Keeps track of certain application-level state attributes
	 */
	public class TimeoutProxy extends Proxy implements IProxy
	{
		public static const NAME					:String = "TimeoutProxy";
		
		private var _timer:Timer;
		
		public function TimeoutProxy( data:Object = null ) {
			super( NAME, data );
		}
		
		/**
		 * How long in seconds the app will wait to reset itself. 
		 * This is about the app returning to the Home screen after it's been idle for an amount of time.
		 */
		public function setDurationInSeconds(value:Number):void {
			if (_timer){
				_timer.stop()
				_timer.removeEventListener(TimerEvent.TIMER_COMPLETE, onTimerComplete);
				_timer = null
			}
			_timer = new Timer(value * 1000,1);
			_timer.addEventListener(TimerEvent.TIMER_COMPLETE, onTimerComplete);
		}
		
		private function onTimerComplete(event:TimerEvent):void {
			trace("onTimerComplete");
			sendNotification(Notifications.APP_IDLE_TIMEOUT);
		}
		
		public function stop():void {
			trace("timer stopped. Total duration: " + _timer.delay + ", is running: " + _timer.running); 
			_timer.stop()
			_timer.reset();
		}
		
		public function start():void {
			trace("timer started. Total duration: " + _timer.delay + ", is running: " + _timer.running); 
			_timer.start();
		}
	}
}