package ca.stationsquare.wallmaps.model
{
	public class ScreenID
	{
		public static const HOME:String = "home"; 
		public static const MAP:String = "map";
	}
}