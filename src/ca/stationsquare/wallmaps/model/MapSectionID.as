package ca.stationsquare.wallmaps.model
{
	public class MapSectionID
	{
		public static const TRANSIT:String = "transit";
		public static const SHOPPING:String = "shopping";
		public static const DINING:String = "dining";
		public static const PERSONAL_SERVICES:String = "personalServices";
		public static const EDUCATION:String = "education";
		public static const ENTERTAINMENT:String = "entertainment";
	}
}