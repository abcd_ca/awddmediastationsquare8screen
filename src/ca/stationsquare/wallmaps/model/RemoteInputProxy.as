package ca.stationsquare.wallmaps.model
{
	import ca.stationsquare.wallmaps.Notifications;
	
	import flash.events.ProgressEvent;
	import flash.events.ServerSocketConnectEvent;
	import flash.events.TimerEvent;
	import flash.geom.Point;
	import flash.net.InterfaceAddress;
	import flash.net.NetworkInfo;
	import flash.net.NetworkInterface;
	import flash.net.ServerSocket;
	import flash.net.Socket;
	import flash.utils.ByteArray;
	import flash.utils.Timer;
	
	import org.puremvc.as3.interfaces.IProxy;
	import org.puremvc.as3.patterns.proxy.Proxy;
	
	/**
	 * Keeps track of certain application-level state attributes
	 */
	public class RemoteInputProxy extends Proxy implements IProxy
	{
		public static const NAME					:String = "TouchInputProxy";
		private var _serverSocket:ServerSocket = new ServerSocket();
		private var _clientSocket:Socket;
		private var _screenIDMap:Object;
//		private var _devTimer:Timer = new Timer(1000,0);
		
		public function RemoteInputProxy( data:Object = null ) {
			super( NAME, data );
			_screenIDMap = {
				A0:{
					offsetX:0,
					offsetY:0
				},
				B0:{
					offsetX:960,
					offsetY:0
				},
				A1:{
					offsetX:0,
					offsetY:540
				},
				B1:{
					offsetX:960,
					offsetY:540
				},
				A2:{
					offsetX:0,
					offsetY:540 * 2
				},
				B2:{
					offsetX:960,
					offsetY:540 * 2
				},
				A3:{
					offsetX:0,
					offsetY:540 * 3
				},
				B3:{
					offsetX:960,
					offsetY:540 * 3
				},
				A4:{
					offsetX:0,
					offsetY:540 * 4
				},
				B4:{
					offsetX:960,
					offsetY:540 * 4
				}
			}
//			_devTimer.addEventListener(TimerEvent.TIMER, onDevTimer);
		}
		
		public function startServer(port:int):void {
			this.bind(port);
//			_devTimer.start();
		}
		
		private function getMyLocalIP():String{
			var netInterfaces:Vector.<NetworkInterface> = NetworkInfo.networkInfo.findInterfaces();
			var addresses:Vector.<InterfaceAddress> = netInterfaces[1].addresses;
			return addresses[0].address;
		}
		
		private function onConnect( event:ServerSocketConnectEvent ):void
		{
			_clientSocket = event.socket;
			_clientSocket.addEventListener( ProgressEvent.SOCKET_DATA, onClientSocketData );
			log( "Connection from " + _clientSocket.remoteAddress + ":" + _clientSocket.remotePort );
		}
		
		private function onClientSocketData( event:ProgressEvent ):void
		{
			var buffer:ByteArray = new ByteArray();
			_clientSocket.readBytes( buffer, 0, _clientSocket.bytesAvailable );
			var messageReceived:String = buffer.toString();
			var point:Point;
			log( "Received: " +  messageReceived);
			try{
				var o:Object = JSON.parse(messageReceived);
				log("");
				log("Parts of deserialized json object:");
				for (var key:String in o){
					log("\t" + key + ": " + o[key]);
				}
				log("");
				
				if (o.hasOwnProperty('id') && o.hasOwnProperty('x') && o.hasOwnProperty('y')){
					point = new Point(o.x + _screenIDMap[o.id].offsetX, o.y + _screenIDMap[o.id].offsetY);
				}else{
					log("unexpected data structure");
					throw Error("unexpected data structure");
				}
			}catch (e:Error){
				log("Couldn't parse JSON");
				trace(e);
			}
			
			if (point){
				sendNotification(Notifications.REMOTE_TAP, point);
			}
		}
		
		private function bind(port:int):void {
			try {
				if( _serverSocket.bound ) 
				{
					_serverSocket.close();
					_serverSocket = new ServerSocket();
					
				}
				_serverSocket.bind(port, this.getMyLocalIP());
				_serverSocket.addEventListener( ServerSocketConnectEvent.CONNECT, onConnect );
				_serverSocket.listen();
				log( "Bound to: " + _serverSocket.localAddress + ":" + _serverSocket.localPort );
			}catch (e:Error){
				log( "Couldn't bind to port " + port + " trying " + (port + 1) );
				bind(port + 1);
			}
		}
		
		private function log( text:String ):void
		{
			//TODO make available a log textfield to see on the screen, visibility configurable.
			trace( "Server log: " + text );
			sendNotification(Notifications.SERVER_LOG_MESSAGE, text);
		}
//		
//		private function onDevTimer(event:TimerEvent):void {
//			trace("onDevTimer");
//			sendNotification(Notifications.REMOTE_TAP, new Point(Math.random() * 1920, Math.random() * 2160));
//		}
	}
}