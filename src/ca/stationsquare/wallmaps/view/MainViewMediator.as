package ca.stationsquare.wallmaps.view
{
	import ca.stationsquare.wallmaps.Notifications;
	import ca.stationsquare.wallmaps.model.ConfigProxy;
	import ca.stationsquare.wallmaps.view.component.MainView;
	
	import flash.geom.Point;
	
	import org.puremvc.as3.interfaces.IMediator;
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.mediator.Mediator;

	/**
     * A Mediator for interacting with the application .
     */
    public class MainViewMediator extends Mediator implements IMediator
    {
        // Cannonical name of the Mediator
        public static const NAME:String = "MainViewMediator";
		private var _configProxy:ConfigProxy;
		
        /**
         * Constructor. 
         */
        public function MainViewMediator( viewComponent:Object ) 
        {
            // pass the viewComponent to the superclass where 
            // it will be stored in the inherited viewComponent property
            super( NAME, viewComponent );
//			mainView.addEventListener(ScreenEvent.BLOCK_INPUT, onBlockEvents);
//			mainView.addEventListener(ScreenEvent.UNBLOCK_INPUT, onUnblockEvents);
//			mainView.addEventListener(PlaybackControlsEvent.CLICK_PLAY, onClickPlaybackControlsBtn);
//			mainView.addEventListener(PlaybackControlsEvent.CLICK_PAUSE, onClickPlaybackControlsBtn);
//			mainView.addEventListener(PlaybackControlsEvent.CLICK_REPLAY, onClickPlaybackControlsBtn);
//			mainView.addEventListener(PlaybackControlsEvent.CLICK_NEXT, onClickPlaybackControlsBtn);
        }
		
		override public function onRegister():void {
			super.onRegister();
			_configProxy = facade.retrieveProxy(ConfigProxy.NAME) as ConfigProxy;
			
			if (_configProxy.showServerLog){
				this.mainView.showLog();
			}
		} 

        public function get mainView():MainView {
			return viewComponent as MainView;
        }

        /**
         * List all notifications this Mediator is interested in.
         * <P>
         * Automatically called by the framework when the mediator
         * is registered with the view.</P>
         * 
         * @return Array the list of Nofitication names
         */
        override public function listNotificationInterests():Array 
        {
            return [
						Notifications.SERVER_LOG_MESSAGE,
						Notifications.REMOTE_TAP,
//						Notifications.BLOCK_INPUT,
//						Notifications.UNBLOCK_INPUT,
//						Notifications.ACTIVATE,
                   ];
        }

        /**
         * Handle all notifications this Mediator is interested in.
         * <P>
         * Called by the framework when a notification is sent that
         * this mediator expressed an interest in when registered
         * (see <code>listNotificationInterests</code>.</P>
         * 
         * @param INotification a notification 
         */
        override public function handleNotification( note:INotification ):void 
        {
        	switch ( note.getName()){
        		case Notifications.SERVER_LOG_MESSAGE:
					if (_configProxy.showServerLog) {
						mainView.showLogMessage(note.getBody() as String);
					}
        			break;
				
				case Notifications.REMOTE_TAP:
					if (_configProxy.showGuides){
						mainView.visualizeRemoteTouchPoint(note.getBody() as Point);
					}
					break;
        	}
        }
//		
//		private function onBlockEvents(event:ScreenEvent):void {
//			mainView.showEventBlocker();
//		}
//		
//		private function onUnblockEvents(event:ScreenEvent):void {
//			mainView.hideEventBlocker();
//		}
//		
//		private function onClickPlaybackControlsBtn(event:PlaybackControlsEvent):void {
//			sendNotification(Notifications.PLAY_SOUND_EFFECT, SoundEffectID.BUTTON);
//			switch(event.type){
//				case PlaybackControlsEvent.CLICK_PLAY:
//				case PlaybackControlsEvent.CLICK_REPLAY:
//					trace("click play");
//					sendNotification(Notifications.PLAY_NARRATION);
//					break;
//				
//				case PlaybackControlsEvent.CLICK_PAUSE:
//					sendNotification(Notifications.PAUSE_NARRATION);
//					trace("click pause");
//					break;
//				
////				case PlaybackControlsEvent.CLICK_NEXT:
////					trace("click next");
////					sendNotification(Notifications.SKIP_AHEAD);
//					break;
//				
//			}
//		}
//		
//		/**
//		 * This is the alert that shows when the app is restored from being in the background (iOS)
//		 */
//		private function showResumeAlert():void {
//			if (!_resumeAlert){
//				var soundProxy:SoundProxy = facade.retrieveProxy(SoundProxy.NAME) as SoundProxy;
//				var configProxy:ConfigProxy = facade.retrieveProxy(ConfigProxy.NAME) as ConfigProxy;
//				_resumeAlert = new ResumeAlert();
//				_resumeAlert.addEventListener(ResumeAlertEvent.CLICK_RESUME, onClickResumeApp);
//				_resumeAlert.addEventListener(ResumeAlertEvent.CLICK_RESTART, onClickRestartApp);
//				_resumeAlert.data = configProxy.resumeAlertXML;
//				this.mainView.addChild(_resumeAlert);
//				
//				if (this.mainView.currentScreen is SlideshowScreen){
//					if (soundProxy.narrationClipState == NarrationClipState.PLAYING)
//						sendNotification(Notifications.PAUSE_NARRATION);
//				}else{
//					sendNotification(Notifications.CANCEL_NARRATION);
//				}
//			}
//			
//		}
//		
//		private function removeResumeAlert():void {
//			if (_resumeAlert){
//				this.mainView.removeChild(_resumeAlert);
//				_resumeAlert.removeEventListener(ResumeAlertEvent.CLICK_RESUME, onClickResumeApp);
//				_resumeAlert.removeEventListener(ResumeAlertEvent.CLICK_RESTART, onClickRestartApp);
//				_resumeAlert = null;
//			}
//		}
//		
//		private function onClickResumeApp(event:ResumeAlertEvent):void {
//			this.removeResumeAlert();
//			sendNotification(Notifications.PLAY_SOUND_EFFECT, SoundEffectID.BUTTON);
//			sendNotification(Notifications.RESUME_FROM_BACKGROUND);
//		}
//		
//		private function onClickRestartApp(event:ResumeAlertEvent):void {
//			this.removeResumeAlert();
//			sendNotification(Notifications.PLAY_SOUND_EFFECT, SoundEffectID.BUTTON);
//			sendNotification(Notifications.RESET);
//		}
    }
}