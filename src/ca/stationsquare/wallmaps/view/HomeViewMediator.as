package ca.stationsquare.wallmaps.view
{
	import ca.stationsquare.wallmaps.Notifications;
	import ca.stationsquare.wallmaps.event.HomeViewEvent;
	import ca.stationsquare.wallmaps.model.MapSectionID;
	import ca.stationsquare.wallmaps.model.ScreenID;
	import ca.stationsquare.wallmaps.model.StateProxy;
	
	import flash.geom.Point;
	import flash.utils.Dictionary;
	
	import org.puremvc.as3.interfaces.IMediator;
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.mediator.Mediator;

	/**
     * A Mediator for interacting with the application .
     */
    public class HomeViewMediator extends Mediator implements IMediator
    {
        // Cannonical name of the Mediator
        public static const NAME:String = "HomeViewMediator";
		private var _stateProxy:StateProxy;
		private var _eventSectionMap:Dictionary;
        /**
         * Constructor. 
         */
        public function HomeViewMediator( viewComponent:Object ) 
        {
            // pass the viewComponent to the superclass where 
            // it will be stored in the inherited viewComponent property
            super( NAME, viewComponent );
			homeView.addEventListener(HomeViewEvent.CLICK_TRANSIT, onClickHomeNavBtn);
			homeView.addEventListener(HomeViewEvent.CLICK_SHOPPING, onClickHomeNavBtn);
			homeView.addEventListener(HomeViewEvent.CLICK_DINING, onClickHomeNavBtn);
			homeView.addEventListener(HomeViewEvent.CLICK_PERSONAL_SERVICES, onClickHomeNavBtn);
			homeView.addEventListener(HomeViewEvent.CLICK_EDUCATION, onClickHomeNavBtn);
			homeView.addEventListener(HomeViewEvent.CLICK_ENTERTAINMENT, onClickHomeNavBtn);
//			mainView.addEventListener(ScreenEvent.UNBLOCK_INPUT, onUnblockEvents);
//			mainView.addEventListener(PlaybackControlsEvent.CLICK_PLAY, onClickPlaybackControlsBtn);
//			mainView.addEventListener(PlaybackControlsEvent.CLICK_PAUSE, onClickPlaybackControlsBtn);
//			mainView.addEventListener(PlaybackControlsEvent.CLICK_REPLAY, onClickPlaybackControlsBtn);
//			mainView.addEventListener(PlaybackControlsEvent.CLICK_NEXT, onClickPlaybackControlsBtn);
			
			_eventSectionMap = new Dictionary();
			_eventSectionMap[HomeViewEvent.CLICK_TRANSIT] = MapSectionID.TRANSIT;
			_eventSectionMap[HomeViewEvent.CLICK_SHOPPING] = MapSectionID.SHOPPING;
			_eventSectionMap[HomeViewEvent.CLICK_DINING] = MapSectionID.DINING;
			_eventSectionMap[HomeViewEvent.CLICK_PERSONAL_SERVICES] = MapSectionID.PERSONAL_SERVICES;
			_eventSectionMap[HomeViewEvent.CLICK_EDUCATION] = MapSectionID.EDUCATION;
			_eventSectionMap[HomeViewEvent.CLICK_ENTERTAINMENT] = MapSectionID.ENTERTAINMENT;
        }
		
		override public function onRegister():void {
			super.onRegister();
			_stateProxy = this.facade.retrieveProxy(StateProxy.NAME) as StateProxy;
		} 

        public function get homeView():HomeView {
			return viewComponent as HomeView;
        }

        /**
         * List all notifications this Mediator is interested in.
         * <P>
         * Automatically called by the framework when the mediator
         * is registered with the view.</P>
         * 
         * @return Array the list of Nofitication names
         */
        override public function listNotificationInterests():Array 
        {
            return [
						Notifications.REMOTE_TAP,
                   ];
        }

        /**
         * Handle all notifications this Mediator is interested in.
         * <P>
         * Called by the framework when a notification is sent that
         * this mediator expressed an interest in when registered
         * (see <code>listNotificationInterests</code>.</P>
         * 
         * @param INotification a notification 
         */
        override public function handleNotification( note:INotification ):void 
        {
        	switch ( note.getName()){
        		case Notifications.REMOTE_TAP:
					if (_stateProxy.currentScreenID == ScreenID.HOME){
						this.homeView.remoteTap(note.getBody() as Point);
					}
        			break;
				
        	}
        }
		
		private function onClickHomeNavBtn(event:HomeViewEvent):void {
			trace("tap home button");
			if (!_stateProxy.nextScreenID){
				SFX.play(SFX.BUTTON_BEEP);
			
				//TODO navigate to map screen, living section. I guess set these as next section in the state proxy
	//			Screen.MAP;
	//			MapSection.LIVING;
				
				_stateProxy.nextScreenID = ScreenID.MAP;
				_stateProxy.nextMapSectionID = _eventSectionMap[event.type];
				
				sendNotification(Notifications.NAVIGATE_TO_SCREEN);
			}else{
				trace("ignoring double-tap");
				trace("ignoring double-tap");
			}
		}
    }
}