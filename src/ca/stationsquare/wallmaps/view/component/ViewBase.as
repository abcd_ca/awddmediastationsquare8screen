package ca.stationsquare.wallmaps.view.component
{
	import ca.stationsquare.wallmaps.event.TransitionEvent;
	
	import flash.display.Sprite;
	
	public class ViewBase extends Sprite
	{
		public function ViewBase()
		{
			super();
		}
		
		public function buildUp():void {
			//no-op
		}
		
		public function tearDown():void {
			//no-op
		}
		
		public function tearDownComplete():void {
			this.dispatchEvent(new TransitionEvent(TransitionEvent.TEAR_DOWN_COMPLETE));
		}
		
		public function buildUpComplete():void {
			this.dispatchEvent(new TransitionEvent(TransitionEvent.BUILD_UP_COMPLETE));
		}
	}
}