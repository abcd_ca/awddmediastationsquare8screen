package ca.stationsquare.wallmaps.view.component
{
	import ca.stationsquare.wallmaps.event.TransitionEvent;
	
	import caurina.transitions.Tweener;
	
	import flash.display.Loader;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.geom.Point;
	import flash.net.URLRequest;
	
	public class HomeViewBase extends ViewBase
	{
		private var _bg:KenBurnsSlideshow;
		public var copyA:Sprite;
		public var copyB:Sprite;
		public var nav:HomeNav;
		
		public function HomeViewBase()
		{
			super();
			_bg = new KenBurnsSlideshow();
			this.addChildAt(_bg, 0);
		}
		
		override public function buildUp():void {
			super.buildUp();
			Tweener.addTween(this, {
				_autoAlpha:1,
				time:.5,
				transition:"easeOutSine",
				onComplete:buildUpComplete
			});
		}
		
		override public function tearDown():void {
			Tweener.addTween(this, {
				_autoAlpha:0,
				time:.75,
				transition:"easeOutSine",
				onComplete:tearDownComplete
			});
		}
		
		/**
		 * Do collision detection between given point and any nav, 
		 * effect a click to that button if collision is detected.
		 */ 
		public function remoteTap(point:Point):void {
			nav.remoteTap(point);
		}
		
		private function onIOError(event:IOErrorEvent):void {
			trace(event);
		}
	}
}