package ca.stationsquare.wallmaps.view.component
{
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	
	public class Guides extends Sprite
	{
		private var _logField:TextField;
		
		public function Guides()
		{
			super();
			this.addEventListener(Event.ADDED_TO_STAGE, addedToStage);
		}
		
		private function addedToStage(event:Event):void {
			removeEventListener(Event.ADDED_TO_STAGE, addedToStage);
			var cols:int = 2;
			var rows:int = 4;
			var totalWidth:int = 1920;
			var totalHeight:int = 2160;
			var monitorWidth:Number = totalWidth / cols;
			var monitorHeight:Number = totalHeight / rows;
			
			var colLetterMap:Object = {0:"A",1:"B"};
			var monitorID:String;
			
			this.graphics.lineStyle(1,0xff0000);
			
			//			guides.alpha = .5;
			
			for (var row:int = 0; row < rows; row++){
				for (var col:int = 0; col < cols; col++){
					monitorID = colLetterMap[col] + row;
					this.graphics.drawRect(monitorWidth * col, monitorHeight * row, monitorWidth - 1, monitorHeight - 1);
					var fmt:TextFormat = new TextFormat();
					fmt.size = 40;
					fmt.bold = true;
					fmt.color = 0xff0000;
					
					var tf:TextField = new TextField();
					tf.text = monitorID;
					tf.autoSize = TextFieldAutoSize.LEFT;
					tf.x = monitorWidth * col + 20;
					tf.y = monitorHeight * row + 20;
					tf.setTextFormat(fmt);
					this.addChild(tf);
				}
			}
		}
		
		public function showLog():void {
			_logField = new TextField();
			var pad:int = 40;
			_logField.width = 960 - pad;
			_logField.height = 540 - pad;
			_logField.x = pad / 2;
			_logField.y = pad / 2;
			_logField.border = true;
			_logField.background = true;
			this.addChild(_logField);
		}
		
		public function showLogMessage(message:String):void {
			_logField.appendText( message + "\n" );
			var fmt:TextFormat= new TextFormat();
			fmt.size = 30;
			_logField.setTextFormat(fmt);
			_logField.scrollV = _logField.maxScrollV;
		}
	}
}