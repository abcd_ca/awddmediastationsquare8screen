package ca.stationsquare.wallmaps.view.component
{
	import ca.stationsquare.wallmaps.event.HomeViewEvent;
	import ca.stationsquare.wallmaps.event.MapViewEvent;
	import ca.stationsquare.wallmaps.event.TransitionEvent;
	import ca.stationsquare.wallmaps.model.MapSectionID;
	import ca.stationsquare.wallmaps.view.component.swc.MapNavBase;
	
	import flash.display.DisplayObject;
	import flash.display.SimpleButton;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.ColorTransform;
	import flash.geom.Point;
	import flash.text.TextField;
	import flash.utils.Dictionary;
	
	public class MapNav extends MapNavBase
	{
		private var _buttonEventMap:Dictionary;
		private var _currentButton:MapNavButton;
		
		public function MapNav()
		{
			super();
//			zoomMapBtn.addEventListener(MouseEvent.CLICK, onClick);
			transitBtn.addEventListener(MouseEvent.CLICK, onClick);
			shoppingBtn.addEventListener(MouseEvent.CLICK, onClick);
			diningBtn.addEventListener(MouseEvent.CLICK, onClick);
			personalServicesBtn.addEventListener(MouseEvent.CLICK, onClick);
			educationBtn.addEventListener(MouseEvent.CLICK, onClick);
			entertainmentBtn.addEventListener(MouseEvent.CLICK, onClick);
			
			transitBtn.setLabel("TRANSIT");
			shoppingBtn.setLabel("SHOPPING");
			diningBtn.setLabel("DINING");
			personalServicesBtn.setLabel("PERSONAL SERVICES");
			educationBtn.setLabel("EDUCATION");
			entertainmentBtn.setLabel("ENTERTAINMENT");

			var btns:Vector.<Sprite> = new <Sprite>[transitBtn, shoppingBtn, diningBtn, personalServicesBtn, educationBtn, educationBtn];
			for each (var btn:Sprite in btns){
//				btn.mouseChildren = false;
				btn.buttonMode = true;
				btn.addEventListener(MouseEvent.CLICK, onClick);
			}
//			
			_buttonEventMap = new Dictionary();
//			_buttonEventMap[zoomMapBtn] = MapViewEvent.CLICK_HOME;
			_buttonEventMap[transitBtn] = HomeViewEvent.CLICK_TRANSIT;
			_buttonEventMap[shoppingBtn] = HomeViewEvent.CLICK_SHOPPING;
			_buttonEventMap[diningBtn] = HomeViewEvent.CLICK_DINING;
			_buttonEventMap[personalServicesBtn] = HomeViewEvent.CLICK_PERSONAL_SERVICES;
			_buttonEventMap[educationBtn] = HomeViewEvent.CLICK_EDUCATION;
			_buttonEventMap[entertainmentBtn] = HomeViewEvent.CLICK_ENTERTAINMENT;
		}
		
		public function setMapSectionColour(sectionID:String, colour:int):void {
			var btn:MapNavButton;
			switch (sectionID){
				case MapSectionID.TRANSIT:
					btn = transitBtn;
					break;
				
				case MapSectionID.SHOPPING:
					btn = shoppingBtn;
					break;
				
				case MapSectionID.DINING:
					btn = diningBtn;
					break;
				
				case MapSectionID.PERSONAL_SERVICES:
					btn = personalServicesBtn;
					break;
				
				case MapSectionID.EDUCATION:
					btn = educationBtn;
					break;
				
				case MapSectionID.ENTERTAINMENT:
					btn = entertainmentBtn;
					break;
			}
			
			btn.setColour(colour);
		}
		
		private function onClick(event:MouseEvent):void {
			this.dispatchEvent(new MapViewEvent(_buttonEventMap[event.target]));
		}
		
		public function displayAsSelected(mapSectionID:String):void {
			if (_currentButton){
				_currentButton.displayAsSelected(false);
			}
			
			switch (mapSectionID){
				case MapSectionID.TRANSIT:
					_currentButton = transitBtn;
					break;
				
				case MapSectionID.SHOPPING:
					_currentButton = shoppingBtn;
					break;
				
				case MapSectionID.DINING:
					_currentButton = diningBtn;
					break;
				
				case MapSectionID.PERSONAL_SERVICES:
					_currentButton = personalServicesBtn;
					break;
				
				case MapSectionID.EDUCATION:
					_currentButton = educationBtn;
					break;
				
				case MapSectionID.ENTERTAINMENT:
					_currentButton = entertainmentBtn;
					break;
			}
			
			_currentButton.displayAsSelected(true);
		}
		
		public function remoteTap(point:Point):void {
			for (var key:Object in _buttonEventMap) {
				var btn:DisplayObject = key as DisplayObject;
				if (btn.hitTestPoint(point.x, point.y)){
					this.dispatchEvent(new MapViewEvent(_buttonEventMap[btn]));
				}
			}
		}
	}
}