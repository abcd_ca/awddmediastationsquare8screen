package ca.stationsquare.wallmaps.view.component
{
	import ca.stationsquare.wallmaps.event.DevEvent;
	import ca.stationsquare.wallmaps.event.MapViewEvent;
	import ca.stationsquare.wallmaps.event.TransitionEvent;
	import ca.stationsquare.wallmaps.model.MapSectionID;
	
	import caurina.transitions.Tweener;
	
	import flash.display.Bitmap;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.net.URLRequest;
	
	public class MapView extends ViewBase
	{
//		private var _mapLoader:Loader;
		private var _macroMapContainer:Sprite;
		private var _microMapContainer:Sprite;
		private var _mapNav:MapNav;
		private var _zoomMapBtn:ZoomMapButton;
		private var _mapPlacemarksListPanel:PlacemarkListPanel;
		private var _mapPlacemarksListMask:Shape;
		
		[Embed(source="assets/img/macroMap.png")]
		private var MacroMapPic:Class;
		
		[Embed(source="assets/img/microMap.png")]
		private var MicroMapPic:Class;
		
		private var _macroMapBmp:Bitmap;
		private var _microMapBmp:Bitmap;
		private var _currentMapSectionID:String;
		private var _transitionDotsBG:DotsBackground;
		
		private var _placemarks:Vector.<Placemark>;
		private var _sectionData:XML;
		private var _showGuides:Boolean;
		
		public function MapView(showGuides:Boolean = false)
		{
			super();
			_showGuides = showGuides;
			this.addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			_placemarks = new Vector.<Placemark>();
		}
		
		private function onAddedToStage(event:Event):void {
			this.removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			
			_macroMapBmp = new MacroMapPic as Bitmap;
			_microMapBmp = new MicroMapPic as Bitmap;
			_macroMapBmp.smoothing = true;
			_microMapBmp.smoothing = true;
			
			var scale:Number = 2160 / _macroMapBmp.height;
			_macroMapContainer = new Sprite();
			_macroMapContainer.addChild(_macroMapBmp);
			_macroMapContainer.scaleX = _macroMapContainer.scaleY = scale;
			_macroMapContainer.x = 20;
			
			_mapNav = new MapNav();
			_zoomMapBtn = new ZoomMapButton();
			_zoomMapBtn.addEventListener(MouseEvent.CLICK, onClickZoomBtn);
			_mapPlacemarksListPanel = new PlacemarkListPanel()
			_mapPlacemarksListMask = new Shape();
			_mapPlacemarksListMask.graphics.beginFill(0xff0000);
			_mapPlacemarksListMask.graphics.drawRect(0,0,_mapNav.width, 2160);
//			_mapPlacemarksListMask.alpha = .5;
			_mapPlacemarksListPanel.mask = _mapPlacemarksListMask;
				
			scale = 1920 / _microMapBmp.width;
			_microMapContainer = new Sprite();
			_microMapContainer.addChild(_microMapBmp);
			_microMapContainer.scaleX = _microMapContainer.scaleY = scale;
				
			this.initMapNavPosition();
			//this is a duplicate of the very background, used to support the foreground map fading out without seeing the bottom map where the negative space is.
			_transitionDotsBG = new DotsBackground();
				
			this.addChild(new DotsBackground());
			this.addChild(_microMapContainer);
			this.addChild(_transitionDotsBG);
			this.addChild(_macroMapContainer);
			this.addChild(_mapPlacemarksListPanel);
			this.addChild(_mapPlacemarksListMask);
			this.addChild(_mapNav);
			this.addChild(_zoomMapBtn);
			
//			_mapLoader = new Loader();
//			_mapLoader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, onIOError);
//			_mapLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, loadMapComplete);
		}
		
		private function initMapNavPosition():void {
			var screenRect:Rectangle = new Rectangle(0,0,960,540);
			_mapNav.x = 1920;
			_mapNav.y = screenRect.height * 2;
			_zoomMapBtn.x = _mapNav.x;
			_zoomMapBtn.y = _mapNav.getRect(this).bottom + 3;
			_mapPlacemarksListMask.x = 1920 - _mapNav.width;
			_mapPlacemarksListMask.y = _mapNav.y - _mapPlacemarksListMask.height;
			_mapPlacemarksListPanel.x = _mapPlacemarksListMask.x;
			_mapPlacemarksListPanel.y = _mapNav.y;
		}
		
//		public function reset():void {
//			var firstRun:Boolean = _macroMapBmp == null;
//			if (!firstRun){
//				var scale:Number = 2160 / _macroMapBmp.height;
//				_macroMapContainer.scaleX = _macroMapContainer.scaleY = scale;
//				_macroMapContainer.x = 20;
//				_macroMapContainer.y = 0;
//				
//				this.initMapNavPosition();
//			}
//		}
		
		public function loadMap():void {
//			_mapLoader.load(new URLRequest("assets/img/macroMap.png"));
		}
		
//		private function loadMapComplete(event:Event):void {
//			var scale:Number = 2160 / _mapLoader.height;
//			var _macroMap:Bitmap = Bitmap(_mapLoader.content);
//			_macroMap.smoothing = true;
//			_macroMapContainer.addChild(_macroMap);
//			_macroMapContainer.scaleX = _macroMapContainer.scaleY = scale;
//			_macroMapContainer.x = 20;
//		}
		
		override public function buildUp():void {
//			_mapNav.displayAsSelected(_currentMapSectionID);
			
			Tweener.addTween(_macroMapContainer, {
				scaleX:1,
				scaleY:1,
				x:-1953,
				y:-483,
				time:1.5,
				delay:1.5,
				transition:"easeInOutCirc"
				//				onComplete:fadeOutHomePicComplete
			});
			
			Tweener.addTween(_macroMapContainer, {
				time:.75,
				delay:2.25,
				_autoAlpha:0,
				transition:"easeOutCirc"
				//				onComplete:fadeOutHomePicComplete
			});

			Tweener.addTween(_transitionDotsBG, {
				_autoAlpha:0,
				time:1.5,
				delay:1.5,
				transition:"easeInOutCirc"
//				onComplete:fadeOutTransitionDotsComplete
			});
			
			Tweener.addTween(_microMapContainer, {
				scaleX:1,
				scaleY:1,
				x:-(_microMapBmp.width - 1920) / 2,
				y:-(_microMapBmp.height - 2160) / 2,
				time:1.5,
				delay:1.5,
				transition:"easeInOutCirc"
				//				onComplete:zoomToMacroViewComplete
			});
			
			var zoomRect:ZoomRect = new ZoomRect();
			zoomRect.x = 2180 * _macroMapContainer.scaleX + zoomRect.width / 2 - 50;
			zoomRect.y = 1608 * _macroMapContainer.scaleY + zoomRect.height / 2 - 50;
			zoomRect.scaleX *= 3;
			zoomRect.scaleY *= 3;
			zoomRect.alpha = 0;
			this.addChild(zoomRect);
			
			Tweener.addTween(zoomRect, {
				scaleX:_macroMapContainer.scaleX,
				scaleY:_macroMapContainer.scaleY,
				_autoAlpha:1,
				time:.75,
				delay:.5,
				transition:"easeOutBounce"
				//				onComplete:fadeOutHomePicComplete
			});
			
			Tweener.addTween(zoomRect, {
				scaleX:3,
				scaleY:3,
				_autoAlpha:0,
				time:.5,
				delay:1.5,
				transition:"easeIn"
				//				onComplete:fadeOutHomePicComplete
			});
			
			Tweener.addTween(_mapNav, {
				x:_mapNav.x - _mapNav.width,
				time:.3,
				delay:3,
				transition:"easeOutSine"
//				onComplete:onRevealMapNavComplete
			});

			Tweener.addTween(_zoomMapBtn, {
				x:_mapNav.x - _mapNav.width,
				time:.3,
				delay:3,
				transition:"easeOutSine"
//				onComplete:onRevealMapNavComplete
			});
			
			Tweener.addTween(_mapPlacemarksListPanel, {
				y:Math.max(0,_mapNav.y - _mapPlacemarksListPanel.getHeightBasedOnContent()),
				time:.5,
				delay:3.3,
				transition:"easeOutCirc",
				onComplete:onRevealMapNavComplete
			});
		}
		
		private function onRevealMapNavComplete():void {
			this.buildUpComplete();
		}
		
		override public function tearDown():void {
			Tweener.addTween(_mapPlacemarksListPanel, {
				y:_mapNav.y,
				time:.250,
//				delay:3,
				transition:"easeInSine"
				//				onComplete:fadeOutHomePicComplete
			});
			
			Tweener.addTween(_mapNav, {
				x:1920,
				time:.5,
				delay:.25,
				transition:"easeInOutSine"
				//				onComplete:fadeOutHomePicComplete
			});
			
			Tweener.addTween(_zoomMapBtn, {
				x:1920,
				time:.5,
				delay:.25,
				transition:"easeInOutSine"
			});
			
			var scale:Number = 2160 / _macroMapBmp.height;
			Tweener.addTween(_macroMapContainer, {
				scaleX:scale,
				scaleY:scale,
				x:20,
				y:0,
				time:.5,
				delay:.75,
				transition:"easeOutCirc",
				onComplete:tearDownComplete
			});
			
			Tweener.addTween(_macroMapContainer, {
				time:.5,
				delay:.5,
				_autoAlpha:1,
				transition:"easeOutCirc"
				//				onComplete:fadeOutHomePicComplete
			});
			
			Tweener.addTween(_transitionDotsBG, {
				_autoAlpha:1,
				time:.5,
				delay:.75,
				transition:"easeInOutCirc"
				//				onComplete:fadeOutTransitionDotsComplete
			});

			scale = 1920 / _microMapBmp.width;
			Tweener.addTween(_microMapContainer, {
				scaleX:scale,
				scaleY:scale,
				x:0,
				y:0,
				time:.5,
				delay:.75,
				transition:"easeInOutCirc"
				//				onComplete:zoomToMacroViewComplete
			});
		}
		
		override public function tearDownComplete():void {
			_zoomMapBtn.gotoAndStop("toVancouver");
			super.tearDownComplete();
		}
		
		/**
		 * Not part of buildup, this is about letting the user zoom in
		 * to the StationSquare area
		 */
		public function zoomToBurnaby():void {
			trace("MapView#zoomToBurnaby");
			_zoomMapBtn.gotoAndStop("toVancouver");
			Tweener.addTween(_macroMapContainer, {
				scaleX:1,
				scaleY:1,
				x:-2125,
				y:-395,
				time:1.5,
				transition:"easeInOutCirc"
				//				onComplete:fadeOutHomePicComplete
			});
			
			Tweener.addTween(_macroMapContainer, {
				time:.75,
				delay:.75,
				_autoAlpha:0,
				transition:"easeOutCirc"
				//				onComplete:fadeOutHomePicComplete
			});
			
			Tweener.addTween(_transitionDotsBG, {
				_autoAlpha:0,
				time:1.5,
				transition:"easeInOutCirc"
				//				onComplete:fadeOutTransitionDotsComplete
			});
			
			Tweener.addTween(_microMapContainer, {
				scaleX:1,
				scaleY:1,
				x:-(_microMapBmp.width - 1920) / 2,
				y:-(_microMapBmp.height - 2160) / 2,
				time:1.5,
				transition:"easeInOutCirc"
				//				onComplete:zoomToMacroViewComplete
			});
			
			Tweener.addTween(_mapNav, {
				x:1920 - _mapNav.width,
				time:.3,
				delay:1.5,
				transition:"easeOutSine"
				//				onComplete:onRevealMapNavComplete
			});
			
			Tweener.addTween(_zoomMapBtn, {
				x:1920 - _mapNav.width,
				time:.3,
				delay:1.5,
				transition:"easeOutSine"
				//				onComplete:onRevealMapNavComplete
			});
			
			Tweener.addTween(_mapPlacemarksListPanel, {
				y:Math.max(0,_mapNav.y - _mapPlacemarksListPanel.getHeightBasedOnContent()),
				time:.5,
				delay:1.8,
				transition:"easeOutCirc",
				onComplete:onRevealMapNavComplete
			});
		}
		
		/**
		 * Not part of teardown, this is about letting the user zoom out 
		 * to see all of vancouver but staying in the map view
		 */
		public function zoomOutToVancouver():void {
			_zoomMapBtn.gotoAndStop("toBurnaby");
			Tweener.addTween(_mapPlacemarksListPanel, {
				y:_mapNav.y,
				time:.250,
				//				delay:3,
				transition:"easeInSine"
				//				onComplete:fadeOutHomePicComplete
			});
			
			Tweener.addTween(_mapNav, {
				x:1920,
				time:.5,
				delay:.25,
				transition:"easeInOutSine"
				//				onComplete:fadeOutHomePicComplete
			});
			
			var scale:Number = 2160 / _macroMapBmp.height;
			Tweener.addTween(_macroMapContainer, {
				scaleX:scale,
				scaleY:scale,
				x:20,
				y:0,
				time:.5,
				delay:.75,
				transition:"easeOutCirc"
//				onComplete:tearDownComplete
			});
			
			Tweener.addTween(_macroMapContainer, {
				time:.5,
				delay:.5,
				_autoAlpha:1,
				transition:"easeOutCirc"
				//				onComplete:fadeOutHomePicComplete
			});
			
			Tweener.addTween(_transitionDotsBG, {
				_autoAlpha:1,
				time:.5,
				delay:.75,
				transition:"easeInOutCirc"
				//				onComplete:fadeOutTransitionDotsComplete
			});
			
			scale = 1920 / _microMapBmp.width;
			Tweener.addTween(_microMapContainer, {
				scaleX:scale,
				scaleY:scale,
				x:0,
				y:0,
				time:.5,
				delay:.75,
				transition:"easeInOutCirc"
				//				onComplete:zoomToMacroViewComplete
			});
		}
		
		public function set currentMapSectionID(value:String):void {
			_currentMapSectionID = value;
			_mapNav.displayAsSelected(value);
		}
		
		public function tearDownSection():void {
			if (_placemarks.length > 0){
				for each(var placemarkCircle:Placemark in _placemarks){
					Tweener.removeTweens(placemarkCircle);
					if (_microMapContainer.contains(placemarkCircle)){
						_microMapContainer.removeChild(placemarkCircle);
					}
				}
				_placemarks = new Vector.<Placemark>();
				tearDownSectionComplete();
			}else{
				tearDownSectionComplete();
			}
		}
		
		private function tearDownSectionComplete():void {
			this.dispatchEvent(new TransitionEvent(TransitionEvent.TEAR_DOWN_SECTION_COMPLETE));
		}
		
		public function setSectionData(sectionData:XML):void {
			_sectionData = sectionData;
//			var colour:uint = uint("0x" + _sectionData.@colour);
			_mapPlacemarksListPanel.setSectionData(sectionData);
		}
		
		public function buildUpSection():void {
			Tweener.addTween(_mapPlacemarksListPanel, {
				y:Math.max(0,_mapNav.y - _mapPlacemarksListPanel.getHeightBasedOnContent()),
				time:.5,
				transition:"easeOutCirc",
				onComplete:drawPlacemarks
			});
		}
		
		public function drawPlacemarks():void {
			trace("draw placemarks: ");
			var colour:uint = uint("0x" + _sectionData.@colour);
			
			trace("_sectionData: " +_sectionData);
			var c:uint = 1;
			for each (var placemark:XML in _sectionData..placemark){
				trace("placemark: " + placemark + " at " + placemark.@x + "x" + placemark.@y);
				var placemarkCircle:Placemark = new Placemark();
				if (_showGuides) {
					placemarkCircle.mouseChildren = false;
					placemarkCircle.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDownPlacemarkCircle);
					placemarkCircle.addEventListener(MouseEvent.MOUSE_UP, onMouseUpPlacemarkCircle);
				}
				placemarkCircle.setColour(colour);
				placemarkCircle.setLabel(c.toString());
				placemarkCircle.x = placemark.@x;
				placemarkCircle.y = placemark.@y;
				placemarkCircle.scaleX = placemarkCircle.scaleY = 0;
				_microMapContainer.addChild(placemarkCircle);
				_placemarks.push(placemarkCircle);
				Tweener.addTween(placemarkCircle, {
					scaleX:1,
					scaleY:1,
					time:.5,
					delay:Math.random() * .75,
					transition:"easeOutElastic"
				});
				c++;
			}
		}
		
		//dev handlers for setting placemark locations -------- start
		private function onMouseDownPlacemarkCircle(event:MouseEvent):void {
			Placemark(event.target).startDrag();
		}
		
		private function onMouseUpPlacemarkCircle(event:MouseEvent):void {
			var placemark:Placemark = Placemark(event.target);
			placemark.stopDrag();
			var payload:Object = {};
			payload.index = _placemarks.indexOf(placemark);
			payload.x = Math.round(placemark.x);
			payload.y = Math.round(placemark.y);
			
			this.dispatchEvent(new DevEvent("setPlacemarkLocation", payload));
		}
		//dev handlers for setting placemark locations -------- end
		
		public function remoteTap(point:Point):void {
			if (_zoomMapBtn.hitTestPoint(point.x, point.y)){
				this.dispatchEvent(new MapViewEvent(_zoomMapBtn.currentFrame == 1 ? MapViewEvent.CLICK_ZOOM_TO_VANCOUVER : MapViewEvent.CLICK_ZOOM_TO_BURNABY));
			}else{
				_mapNav.remoteTap(point);
			}
		}
		
		public function setMapSectionColour(sectionID:String, colour:int):void {
			_mapNav.setMapSectionColour(sectionID, colour);
		}
		
		private function onClickZoomBtn(event:MouseEvent):void {
			if (_zoomMapBtn.currentFrame == 1){
				this.dispatchEvent(new MapViewEvent(MapViewEvent.CLICK_ZOOM_TO_VANCOUVER));
			}else{
				this.dispatchEvent(new MapViewEvent(MapViewEvent.CLICK_ZOOM_TO_BURNABY));
			}
		}
		
		private function onIOError(event:IOErrorEvent):void {
			trace(event);
		}
	}
}