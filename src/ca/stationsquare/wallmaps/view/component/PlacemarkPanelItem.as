package ca.stationsquare.wallmaps.view.component
{
	import ca.stationsquare.wallmaps.view.component.swc.PlacemarkPanelItemBase;
	
	import flash.display.Sprite;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	
	public class PlacemarkPanelItem extends PlacemarkPanelItemBase
	{
		public function PlacemarkPanelItem()
		{
			super();
		}
		
		public function setItemNumber(value:uint):void {
			this.numLabel.text = value + ".";
			numLabel.x = this.label.x - (numLabel.width - 13);
			numLabel.y = this.label.y;
			this.addChild(numLabel);
		}
		
		public function setLabel(value:String):void {
			this.label.text = value;
			this.label.autoSize = TextFieldAutoSize.LEFT;
		}
	}
}