package ca.stationsquare.wallmaps.view.component
{
	import ca.stationsquare.wallmaps.event.MapViewEvent;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.ColorTransform;
	import flash.text.TextField;
	
	public class MapNavButton extends Sprite
	{
		public var bg:Sprite;
		public var label:TextField;
		private var _colour:int;
		
		public function MapNavButton()
		{
			super();
			this.bg.alpha = 0;
			this.mouseChildren = false;
			this.buttonMode = true;
		}
		
		public function displayAsSelected(selected:Boolean):void {
			this.bg.alpha = selected ? 1 : 0;
			if (selected){
				var newCol:ColorTransform = new ColorTransform();
				newCol.color = _colour;
				bg.transform.colorTransform = newCol;
			}
			this.mouseEnabled = !selected;
		}
		
		public function setLabel(value:String):void {
			this.label.text = value;
		}
		
		public function setColour(value:int):void {
			trace("MapNavButton#setColour " + value.toString(16));
			_colour = value;
		}
	}
}