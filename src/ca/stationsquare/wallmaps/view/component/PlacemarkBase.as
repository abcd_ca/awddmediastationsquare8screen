package ca.stationsquare.wallmaps.view.component
{
	import caurina.transitions.Tweener;
	
	import flash.display.Sprite;
	import flash.geom.ColorTransform;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	
	public class PlacemarkBase extends Sprite
	{
		public var label:TextField;
		public var bg:Sprite;
		
		public function PlacemarkBase()
		{
			super();
		}
		
		public function setColour(value:int):void {
			var newCol:ColorTransform = new ColorTransform();
			newCol.color = value;
			bg.transform.colorTransform = newCol;
		}
		
		public function setLabel(value:String):void {
			label.text = value;
			label.autoSize = TextFieldAutoSize.CENTER
		}
//		
//		public function tearDown():void {
//			Tweener.addTween(this)
//		}
//		
//		public function buildUp():void {
//			
//		}
	}
}