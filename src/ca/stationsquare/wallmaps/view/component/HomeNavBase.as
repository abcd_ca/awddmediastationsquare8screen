package ca.stationsquare.wallmaps.view.component
{
	import ca.stationsquare.wallmaps.event.HomeViewEvent;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.text.TextField;
	import flash.utils.Dictionary;
	
	public class HomeNavBase extends Sprite
	{
		public var transitBtn:Sprite;
		public var shoppingBtn:Sprite;
		public var diningBtn:Sprite;
		public var personalServicesBtn:Sprite;
		public var educationBtn:Sprite;
		public var entertainmentBtn:Sprite;
		
		private var _buttonEventMap:Dictionary;
		
		public function HomeNavBase()
		{
			super();
			TextField(transitBtn['label']).text = "TRANSIT";
			TextField(shoppingBtn['label']).text = "SHOPPING";
			TextField(diningBtn['label']).text = "DINING";
			TextField(personalServicesBtn['label']).text = "PERSONAL SERVICES";
			TextField(educationBtn['label']).text = "EDUCATION";
			TextField(entertainmentBtn['label']).text = "ENTERTAINMENT";

			var btns:Vector.<Sprite> = new <Sprite>[transitBtn, shoppingBtn, diningBtn, personalServicesBtn, educationBtn, entertainmentBtn];
			for each (var btn:Sprite in btns){
				btn.mouseChildren = false;
				btn.buttonMode = true;
				btn.addEventListener(MouseEvent.CLICK, onClick);
			}
			
			_buttonEventMap = new Dictionary();
			_buttonEventMap[transitBtn] = HomeViewEvent.CLICK_TRANSIT;
			_buttonEventMap[shoppingBtn] = HomeViewEvent.CLICK_SHOPPING;
			_buttonEventMap[diningBtn] = HomeViewEvent.CLICK_DINING;
			_buttonEventMap[personalServicesBtn] = HomeViewEvent.CLICK_PERSONAL_SERVICES;
			_buttonEventMap[educationBtn] = HomeViewEvent.CLICK_EDUCATION;
			_buttonEventMap[entertainmentBtn] = HomeViewEvent.CLICK_ENTERTAINMENT;
		}
		
		public function remoteTap(point:Point):void {
			for (var key:Object in _buttonEventMap) {
				var btn:Sprite = key as Sprite;
				if (btn.hitTestPoint(point.x, point.y)){
					this.dispatchEvent(new HomeViewEvent(_buttonEventMap[btn]));
				}
			}
		}
		
		private function onClick(event:MouseEvent):void {
			this.dispatchEvent(new HomeViewEvent(_buttonEventMap[event.target]));
		}
	}
}