package ca.stationsquare.wallmaps.view.component
{
	import ca.stationsquare.wallmaps.view.component.swc.PlacemarkListPanelBase;
	
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	
	public class PlacemarkListPanel extends PlacemarkListPanelBase
	{
		private static const PADDING_TOP_AND_BOTTOM:Number = 47;
		private var _lowestItemY:Number;
		
		public function PlacemarkListPanel()
		{
			super();
		}
	
		public function setHeight(height:Number):void {
			this.bg.height = height;
		}
		
		/**
		 * @param sectionXMLSection node containing placemark nodes
		 */
		public function setSectionData(sectionXML:XML):void {
			//remove any preexisting label items
			_lowestItemY = 0;
			var i:uint = this.numChildren;
			while (i--){
				var child:DisplayObject = this.getChildAt(i);
				if (this.getChildAt(i) != this.bg){
					this.removeChild(child);
				}
			}
			if (sectionXML){
				const COL_1_X:Number = 37;
				const COL_2_X:Number = 182;
				var labelGroup1:Vector.<PlacemarkPanelItem> = new Vector.<PlacemarkPanelItem>();
				var labelGroup2:Vector.<PlacemarkPanelItem> = new Vector.<PlacemarkPanelItem>();
				var groupOneMaxChildren:int = Math.ceil(sectionXML..placemark.length() / 2);
				
				// overall count
				var itemCount:int = 0;
				
				var currentLabelGroup:Vector.<PlacemarkPanelItem> = labelGroup1;
				
				var lastPanelItem:PlacemarkPanelItem;
				
				for each (var placemark:XML in sectionXML..placemark){
					var panelItem:PlacemarkPanelItem = new PlacemarkPanelItem();
					panelItem.setLabel(placemark);
					currentLabelGroup.push(panelItem);
	
					panelItem.x = currentLabelGroup == labelGroup1 ? COL_1_X : COL_2_X;
					panelItem.y = lastPanelItem ? lastPanelItem.getRect(this).bottom + 10 : PADDING_TOP_AND_BOTTOM
					
					this.addChild(panelItem);
					
					if (currentLabelGroup == labelGroup1){
						if (panelItem.getRect(this).bottom > this.bg.height - PADDING_TOP_AND_BOTTOM){
							labelGroup1.pop()
							currentLabelGroup = labelGroup2;
							currentLabelGroup.push(panelItem)
							panelItem.x = COL_2_X;
							panelItem.y = PADDING_TOP_AND_BOTTOM;
							lastPanelItem = panelItem;
						}else if (labelGroup1.length >= groupOneMaxChildren){
							lastPanelItem = null;
							currentLabelGroup = labelGroup2;
						}else{
							lastPanelItem = panelItem;
						}
					}else{
						lastPanelItem = panelItem;
					}
					
					itemCount++;
	
					panelItem.setItemNumber(itemCount);
					
					_lowestItemY = Math.max(panelItem.getRect(this).bottom, _lowestItemY);
				}
			}
		}
		
		public function getHeightBasedOnContent():Number {
			return _lowestItemY + PADDING_TOP_AND_BOTTOM;
		}
	}
}