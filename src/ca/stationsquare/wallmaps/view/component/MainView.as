package ca.stationsquare.wallmaps.view.component
{
	import ca.stationsquare.wallmaps.view.component.HomeNavBase;
	
	import caurina.transitions.Tweener;
	import caurina.transitions.properties.DisplayShortcuts;
	
	import flash.display.Bitmap;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.filters.DropShadowFilter;
	import flash.geom.Point;
	
	public class MainView extends Sprite
	{
		private var _guides:Guides;
		private var _showGuides:Boolean;
		
		public var mapView:MapView;
		public var homeView:HomeView;
		public var currentView:ViewBase;
		
		public function MainView(showGuides:Boolean = false)
		{
			super();
			_showGuides = showGuides;
			this.addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		private function onAddedToStage(event:Event):void {
			this.removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);

			this.homeView = new HomeView();
			this.mapView = new MapView(_showGuides);
			this.currentView = this.homeView;
			
			this.addChild(homeView);
			
			_guides = new Guides();
			_guides.visible = _showGuides;
			this.addChild(_guides);
			this.mapView.loadMap();
		}
		
		private function onIOError(event:IOErrorEvent):void {
			trace(event);
		}
		
		public function showLog():void {
			_guides.showLog();
		}
		
		public function showLogMessage(message:String):void {
			_guides.showLogMessage(message);
		}
		
		public function moveGuidesToFront():void {
			this.addChild(_guides);
		}
		
		public function visualizeRemoteTouchPoint(point:Point):void {
			trace("visualizeRemoteTouchPoint");
			var pointMC:RemoteTouchDot = new RemoteTouchDot();
			pointMC.x = point.x;
			pointMC.y = point.y;
			pointMC.scaleX = pointMC.scaleY = 2;
			pointMC.filters = [new DropShadowFilter()]
			pointMC.addEventListener("animationComplete", this.onTouchPointMCAnimationComplete);
			this.addChild(pointMC);
		}
		
		private function onTouchPointMCAnimationComplete(event:Event):void {
			trace("onTouchPointMCAnimationComplete");
			this.removeChild(event.target as MovieClip);
		}
	}
}