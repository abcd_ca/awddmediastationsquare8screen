package ca.stationsquare.wallmaps.view
{
	import ca.stationsquare.wallmaps.Notifications;
	import ca.stationsquare.wallmaps.event.DevEvent;
	import ca.stationsquare.wallmaps.event.MapViewEvent;
	import ca.stationsquare.wallmaps.model.ConfigProxy;
	import ca.stationsquare.wallmaps.model.MapSectionID;
	import ca.stationsquare.wallmaps.model.ScreenID;
	import ca.stationsquare.wallmaps.model.StateProxy;
	import ca.stationsquare.wallmaps.view.component.MapView;
	
	import flash.events.Event;
	import flash.geom.Point;
	
	import org.puremvc.as3.interfaces.IMediator;
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.mediator.Mediator;

	/**
     * A Mediator for interacting with the application .
     */
    public class MapViewMediator extends Mediator implements IMediator
    {
        // Cannonical name of the Mediator
        public static const NAME:String = "MapViewMediator";
		
		private var _stateProxy:StateProxy;
		private var _configProxy:ConfigProxy;
		
        /**
         * Constructor. 
         */
        public function MapViewMediator( viewComponent:Object ) 
        {
            // pass the viewComponent to the superclass where 
            // it will be stored in the inherited viewComponent property
            super( NAME, viewComponent );
			mapView.addEventListener(MapViewEvent.CLICK_ZOOM_TO_VANCOUVER, onClickZoomToVancouver);
			mapView.addEventListener(MapViewEvent.CLICK_ZOOM_TO_BURNABY, onClickZoomToBurnaby);
			mapView.addEventListener(MapViewEvent.CLICK_TRANSIT, onClickMapSectionBtn);
			mapView.addEventListener(MapViewEvent.CLICK_SHOPPING, onClickMapSectionBtn);
			mapView.addEventListener(MapViewEvent.CLICK_DINING, onClickMapSectionBtn);
			mapView.addEventListener(MapViewEvent.CLICK_PERSONAL_SERVICES, onClickMapSectionBtn);
			mapView.addEventListener(MapViewEvent.CLICK_EDUCATION, onClickMapSectionBtn);
			mapView.addEventListener(MapViewEvent.CLICK_ENTERTAINMENT, onClickMapSectionBtn);
			
			//for dev for conveniently setting the location of the placemarks
			mapView.addEventListener("setPlacemarkLocation", onSetPlacemarkLocation);
        }
		
		override public function onRegister():void {
			super.onRegister();
			
			_stateProxy = facade.retrieveProxy(StateProxy.NAME) as StateProxy;
			_configProxy = facade.retrieveProxy(ConfigProxy.NAME) as ConfigProxy;
			
			this.mapView.addEventListener(Event.ADDED_TO_STAGE, onMapViewAddedToStage);
		} 

        public function get mapView():MapView {
			return viewComponent as MapView;
        }

        /**
         * List all notifications this Mediator is interested in.
         * <P>
         * Automatically called by the framework when the mediator
         * is registered with the view.</P>
         * 
         * @return Array the list of Nofitication names
         */
        override public function listNotificationInterests():Array 
        {
            return [
						Notifications.REMOTE_TAP,
                   ];
        }

        /**
         * Handle all notifications this Mediator is interested in.
         * <P>
         * Called by the framework when a notification is sent that
         * this mediator expressed an interest in when registered
         * (see <code>listNotificationInterests</code>.</P>
         * 
         * @param INotification a notification 
         */
        override public function handleNotification( note:INotification ):void 
        {
        	switch ( note.getName()){
				case Notifications.REMOTE_TAP:
					if (_stateProxy.currentScreenID == ScreenID.MAP){
						this.mapView.remoteTap(note.getBody() as Point);
					}
					break;
        	}
        }
		
		private function onClickZoomToBurnaby(event:MapViewEvent):void {
			SFX.play(SFX.BUTTON_BEEP);
			
			this.mapView.zoomToBurnaby();
		}
		
		private function onClickZoomToVancouver(event:MapViewEvent):void {
			SFX.play(SFX.BUTTON_BEEP);
			
			this.mapView.zoomOutToVancouver();
			//TODO navigate to map screen, living section. I guess set these as next section in the state proxy
			//			Screen.MAP;
			//			MapSection.LIVING;
			
//			_stateProxy.nextScreenID = ScreenID.HOME;
//			_stateProxy.nextMapSectionID = null;
//			
//			sendNotification(Notifications.NAVIGATE_TO_SCREEN, ScreenID.HOME);
		}
		
		private function onClickMapSectionBtn(event:MapViewEvent):void {
			SFX.play(SFX.BUTTON_BEEP);
			switch(event.type){
				case MapViewEvent.CLICK_TRANSIT:
					_stateProxy.nextMapSectionID = MapSectionID.TRANSIT;
					break;
				
				case MapViewEvent.CLICK_SHOPPING:
					_stateProxy.nextMapSectionID = MapSectionID.SHOPPING;
					break;
				
				case MapViewEvent.CLICK_DINING:
					_stateProxy.nextMapSectionID = MapSectionID.DINING;
					break;
				
				case MapViewEvent.CLICK_PERSONAL_SERVICES:
					_stateProxy.nextMapSectionID = MapSectionID.PERSONAL_SERVICES;
					break;
				
				case MapViewEvent.CLICK_EDUCATION:
					_stateProxy.nextMapSectionID = MapSectionID.EDUCATION;
					break;
				
				case MapViewEvent.CLICK_ENTERTAINMENT:
					_stateProxy.nextMapSectionID = MapSectionID.ENTERTAINMENT;
					break;
			}

			sendNotification(Notifications.NAVIGATE_TO_MAP_SECTION);
		}
		
		private function onMapViewAddedToStage(event:Event):void {
			this.mapView.removeEventListener(Event.ADDED_TO_STAGE, onMapViewAddedToStage);
			
			this.mapView.setMapSectionColour(MapSectionID.TRANSIT, uint("0x" + _configProxy.getSectionDataForSectionID(MapSectionID.TRANSIT).@colour))
			this.mapView.setMapSectionColour(MapSectionID.SHOPPING, uint("0x" + _configProxy.getSectionDataForSectionID(MapSectionID.SHOPPING).@colour))
			this.mapView.setMapSectionColour(MapSectionID.DINING, uint("0x" + _configProxy.getSectionDataForSectionID(MapSectionID.DINING).@colour))
			this.mapView.setMapSectionColour(MapSectionID.PERSONAL_SERVICES, uint("0x" + _configProxy.getSectionDataForSectionID(MapSectionID.PERSONAL_SERVICES).@colour))
			this.mapView.setMapSectionColour(MapSectionID.EDUCATION, uint("0x" + _configProxy.getSectionDataForSectionID(MapSectionID.EDUCATION).@colour))
			this.mapView.setMapSectionColour(MapSectionID.ENTERTAINMENT, uint("0x" + _configProxy.getSectionDataForSectionID(MapSectionID.ENTERTAINMENT).@colour))
		}
		//for dev for conveniently positioning the placemarks
		private function onSetPlacemarkLocation(event:DevEvent):void {
			_configProxy.updatePlacemarkLocation(_stateProxy.currentMapSectionID, event.data.index, event.data.x, event.data.y);
		}
    }
}