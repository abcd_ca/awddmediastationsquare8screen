package ca.stationsquare.wallmaps.view
{
	import ca.stationsquare.wallmaps.Notifications;
	import ca.stationsquare.wallmaps.model.MapSectionID;
	import ca.stationsquare.wallmaps.model.ScreenID;
	import ca.stationsquare.wallmaps.model.StateProxy;
	
	import flash.display.Sprite;
	
	import org.puremvc.as3.interfaces.IMediator;
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.mediator.Mediator;

	/**
     * A Mediator for interacting with the application .
     */
    public class ApplicationMediator extends Mediator implements IMediator
    {
        // Cannonical name of the Mediator
        public static const NAME:String = "ApplicationMediator";
		
		private var _stateProxy:StateProxy;
		
        /**
         * Constructor. 
         */
        public function ApplicationMediator( viewComponent:Object ) 
        {
            // pass the viewComponent to the superclass where 
            // it will be stored in the inherited viewComponent property
            super( NAME, viewComponent );
//			app.stage.addEventListener(StageOrientationEvent.ORIENTATION_CHANGE, onOrientationChange);
//			NativeApplication.nativeApplication.addEventListener(Event.ACTIVATE, onActivate);
			
//			//for dev to test Activate event.
//			if (ScreenUtil.isDesktopADL){
//				app.stage.doubleClickEnabled = true;
//				app.stage.addEventListener(MouseEvent.DOUBLE_CLICK, onClickBG);
//			}
        }
		
		override public function onRegister():void {
			super.onRegister();
			_stateProxy = facade.retrieveProxy(StateProxy.NAME) as StateProxy;
			_stateProxy.currentScreenID = ScreenID.HOME;
		} 

        public function get app():Sprite {
			return viewComponent as Sprite;
        }

        /**
         * List all notifications this Mediator is interested in.
         * <P>
         * Automatically called by the framework when the mediator
         * is registered with the view.</P>
         * 
         * @return Array the list of Nofitication names
         */
        override public function listNotificationInterests():Array 
        {
            return [
						Notifications.STARTUP_COMPLETE,
						Notifications.APP_IDLE_TIMEOUT,
//						Notifications.BLOCK_INPUT,
//						Notifications.UNBLOCK_INPUT,
                   ];
        }

        /**
         * Handle all notifications this Mediator is interested in.
         * <P>
         * Called by the framework when a notification is sent that
         * this mediator expressed an interest in when registered
         * (see <code>listNotificationInterests</code>.</P>
         * 
         * @param INotification a notification 
         */
        override public function handleNotification( note:INotification ):void 
        {
        	switch ( note.getName()){
        		case Notifications.STARTUP_COMPLETE:
					trace("app mediator startupComplete");
					//dev fast forward -------- start
//					_stateProxy.nextScreenID = ScreenID.MAP;
//					_stateProxy.nextMapSectionID = MapSectionID.TRANSIT;
//					
//					sendNotification(Notifications.NAVIGATE_TO_SCREEN);
					//dev fast forward -------- end
        			break;
				
				case Notifications.APP_IDLE_TIMEOUT:
					if (_stateProxy.currentScreenID != ScreenID.HOME){
						trace("App idle timeout");
						//navigate back to Home view.
						_stateProxy.nextScreenID = ScreenID.HOME;
						_stateProxy.nextMapSectionID = null;
						sendNotification(Notifications.NAVIGATE_TO_SCREEN);
					}
					break;
        	}
        }
//		
//		private function onOrientationChange(event:StageOrientationEvent):void {
////			trace(stage.deviceOrientation);
//			switch(app.stage.deviceOrientation){
//				case StageOrientation.DEFAULT:
////					trace("forcing rotate right");
//					app.stage.setOrientation(StageOrientation.ROTATED_RIGHT);
//					break;
//				
//				case StageOrientation.UPSIDE_DOWN:
////					trace("forcing rotate left");
//					app.stage.setOrientation(StageOrientation.ROTATED_LEFT);
//					break;
//			}
//		}
//		
//		private function enableSwipeGestures():void {
//			Multitouch.inputMode = MultitouchInputMode.GESTURE;
//			if (!app.stage.hasEventListener(TransformGestureEvent.GESTURE_SWIPE))
//				app.stage.addEventListener (TransformGestureEvent.GESTURE_SWIPE, swipeHandler);
//		}
		
//		private function disableSwipeGestures():void {
//			if (app.stage.hasEventListener(TransformGestureEvent.GESTURE_SWIPE))
//				app.stage.removeEventListener (TransformGestureEvent.GESTURE_SWIPE, swipeHandler);
//		}
		
//		private function swipeHandler(event:TransformGestureEvent):void
//		{
//			switch(event.offsetX)
//			{
//				case 1:
//				{
//					// swiped right
////					trace("swiped right, RESET");
//					sendNotification(Notifications.RESET);
//					break;
//				}
//				case -1:
//				{
//					// swiped left
////					trace("swiped left, SKIP_AHEAD");
//					sendNotification(Notifications.SKIP_AHEAD);
//					break;
//				}
//			}
			
//			switch(event.offsetY)
//			{
//				case 1:
//				{
//					// swiped down
//					break;
//				}
//				case -1:
//				{
//					// swiped up
//					break;
//				}
//			}
//		}
//		
//		private function onActivate(event:Event):void {
//			trace("onActivate");
//			if (_stateProxy.firstActivation){
//				_stateProxy.firstActivation = false;
//			}else{
//				if (!_stateProxy.isNavigating){
//					sendNotification(Notifications.BLOCK_INPUT);
//					sendNotification(Notifications.ACTIVATE);
//				}
//			}
//		}
		
		//dev for testing Acivate event in ADL.
//		private function onClickBG(event:MouseEvent):void {
//			this.onActivate(new Event(Event.ACTIVATE));
//		}
    }
}