package ca.stationsquare.wallmaps 
{
	import ca.stationsquare.wallmaps.Notifications;
	import ca.stationsquare.wallmaps.controller.StartupCommand;
	import ca.stationsquare.wallmaps.controller.navigate.NavigateToMapSectionCommand;
	import ca.stationsquare.wallmaps.controller.navigate.NavigateToScreenCommand;
	
	import org.puremvc.as3.interfaces.IFacade;
	import org.puremvc.as3.patterns.facade.Facade;

	public class ApplicationFacade extends Facade implements IFacade
	{
		private static var facade:ApplicationFacade;

    	public function ApplicationFacade()
        {
        }
        
        /**
         * Singleton ApplicationFacade Factory Method
         */
        public static function getInstance() : ApplicationFacade 
		{
            if ( instance == null ) instance = new ApplicationFacade( );
            return instance as ApplicationFacade;
        }
        	
		override protected function initializeController( ) : void 
        {
            super.initializeController();            
            this.registerCommand(Notifications.STARTUP, StartupCommand);
            this.registerCommand(Notifications.NAVIGATE_TO_SCREEN, NavigateToScreenCommand);
            this.registerCommand(Notifications.NAVIGATE_TO_MAP_SECTION, NavigateToMapSectionCommand);
//            this.registerCommand(Notifications.SLIDESHOW_COMPLETE, SlideshowCompleteCommand);
//            this.registerCommand(Notifications.RESET, ResetCommand);
//            this.registerCommand(Notifications.OPEN_CROSS_MARKETING_SITE, OpenCrossMarketingSiteCommand);
//            this.registerCommand(Notifications.SKIP_AHEAD, SkipAheadCommand);
//            this.registerCommand(Notifications.HELP_VIDEO_COMPLETE, HelpVideoCompleteCommand);
//            this.registerCommand(Notifications.RESUME_FROM_BACKGROUND, ResumeFromBackgroundCommand);
        }

		public function startup( stage:Object ):void
        {
        	sendNotification( Notifications.STARTUP, stage );
        }
	}
}

/**  PrivateClass is used to make the 
 * Singleton constructor private 
 * */
class PrivateClass {
    public function PrivateClass() {
    }
}
